import numpy as np
import pandas as pd
import sys
from main_match_signals import syncFrequencies
import matplotlib.pyplot as plt

mins=np.array([44.43448468573079, 26.045496832850702])
maxs=np.array([44.436176574351016, 26.048940957214707])

a = pd.read_csv(sys.argv[1])
print(a.keys())
a["timestamp"] = a["timestamp"].apply(lambda x : round(x / 10**9, 2))
a["latitude"] = a["latitude"].apply(lambda x : x / np.pi * 180)
a["longitude"] = a["longitude"].apply(lambda x : x / np.pi * 180)

x = np.array(a["timestamp"])
frequency = np.median((1 / (x[1 :] - x[0 : -1])))

syncedGpsData = np.load(sys.argv[2])
desiredFreq = 23.98
print(a.shape)
print(syncedGpsData.shape)

b = {}
keys = list(a.keys())[1:]
b = {key : syncFrequencies(a[key], currentFreq=frequency, desiredFreq=desiredFreq) for key in keys}
b = pd.DataFrame(b)
print(b.shape)
print(b["latitude"].shape)

# print(list(b["latitude"][0:5]))
# print(list(syncedGpsData[0:5, 0]))

blah = np.zeros(len(b) - len(syncedGpsData))
for i in range(len(b) - len(syncedGpsData)):
	res = np.abs(b["latitude"][i : i + len(syncedGpsData)] - syncedGpsData[:, 0])
	# print(i, np.sum(res), len(np.where(res > 0.0001)[0]))
	blah[i] = np.sum(res)

print(np.argmin(blah), np.min(blah))
plt.plot(np.arange(len(blah)), blah)
plt.show()

min = np.argmin(blah)
b = b[min : min + len(syncedGpsData)]

b["latitude"] = syncedGpsData[:, 0]
b["longitude"] = syncedGpsData[:, 1]
b["x"] = (b["latitude"] - mins[0]) / (maxs[0] - mins[0])
b["y"] = (b["longitude"] - mins[1]) / (maxs[1] - mins[1])

print(b.shape)
b.to_csv("resynced_signals.csv")