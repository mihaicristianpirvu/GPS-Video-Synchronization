import numpy as np

# @param[in] signal An array that represents the signal
# @param[in] currentFreq The frequency of the signal
# @param[in] desiredFreq The desired frequency of the signal
# @ret The new upsampled signal
def syncFrequencies(signal, currentFreq, desiredFreq):
	if currentFreq < desiredFreq:
		return syncFrequenciesExtrapolate(signal, currentFreq, desiredFreq)
	else:
		return syncFrequenciesInterpolate(signal, currentFreq, desiredFreq)

def syncFrequenciesExtrapolate(signal, currentFreq, desiredFreq):
	assert currentFreq < desiredFreq
	print("Upsampling from freq", currentFreq, "to", desiredFreq)
	currentLen = len(signal)
	signal = np.append(signal, 0)
	currentT = np.arange(0, currentLen, 1 / currentFreq)
	# print(currentT[0 : 10])

	desiredLen = int(currentLen * (desiredFreq / currentFreq))
	desiredT = np.arange(0, desiredLen, 1 / desiredFreq)
	# print(desiredT[0 : 10])

	freqRapp = desiredFreq / currentFreq
	# indexMapping[i] tells which index in the original signal is the left value for interpolation
	# example: for currentFreq=5, desiredFreq=29.97, we have [0, 0, 0, 0, 0, 0, 1, 1, 1...] meaning that first
	#  6 values are to be interpolated using signal[0] and signal[1]. Then, 6th... with signal[1] and signal[2] etc.
	indexMapping = np.uint32(np.arange(0, desiredLen) // freqRapp)
	newSignal = np.zeros((desiredLen, ), dtype=np.float32)
	# print(indexMapping[-10 : ])
	# print(indexMapping.shape)

	for i in range(len(indexMapping)):
		index = indexMapping[i]
		desired = desiredT[i]
		currentLeft = currentT[index]
		valueLeft = signal[index]
		currentRight = currentT[index + 1]
		valueRight = signal[index + 1]

		diff = currentRight - currentLeft
		perecentLeft = (desired - currentLeft) / diff
		percentRight = (currentRight - desired) / diff

		value = signal[index] * perecentLeft + signal[index + 1] * percentRight
		newSignal[i] = value
	return newSignal

def syncFrequenciesInterpolate(signal, currentFreq, desiredFreq):
	print("Downsampling from freq", currentFreq, "to", desiredFreq)
	assert currentFreq > desiredFreq
	freqRapp = desiredFreq / currentFreq

	currentLen = len(signal)
	signal = np.append(signal, 0)
	currentT = np.arange(0, currentLen) / currentFreq
	# print("Current len:", currentLen)
	# print(currentT[0 : 10])

	desiredLen = int(currentLen * freqRapp)
	desiredT = np.arange(0, desiredLen) / desiredFreq
	# print("Desired len:", desiredLen)
	# print(desiredT[0 : 10])	

	indexMapping = np.uint32(np.arange(0, desiredLen) // freqRapp)
	# print(indexMapping.shape)
	# print(indexMapping[0:10])

	# Simplest approach (no linear interpolating) is just using the closest time in the original signal (subsampling).
	newSignal = signal[indexMapping]
	return newSignal

# http://www.ridgesolutions.ie/index.php/2013/11/14/algorithm-to-calculate-speed-from-two-gps-latitude- \
#  and-longitude-points-and-time-difference/
# @param[in] gpsData Array with 3 values for each position: lat, lon, time
# @ret Array with the speed at each position
def getGPSSpeed(gpsData):
	lat1, lon1 = gpsData[0 : -1, 0 : 2].T
	lat2, lon2 = gpsData[1 : , 0 : 2].T
	timeDelta = (gpsData[1 : , 2] - gpsData[0 : -1 , 2]) * 100

	lat1 = lat1 * np.pi / 180.0;
	lon1 = lon1 * np.pi / 180.0;
	lat2 = lat2 * np.pi / 180.0;
	lon2 = lon2 * np.pi / 180.0;
	r = 6378100;

	# P
	rho1 = r * np.cos(lat1);
	z1 = r * np.sin(lat1);
	x1 = rho1 * np.cos(lon1);
	y1 = rho1 * np.sin(lon1);

	# Q
	rho2 = r * np.cos(lat2);
	z2 = r * np.sin(lat2);
	x2 = rho2 * np.cos(lon2);
	y2 = rho2 * np.sin(lon2);

	dot = (x1 * x2 + y1 * y2 + z1 * z2);
	cos_theta = dot / (r * r);

	theta = np.arccos(cos_theta);
	distance =  r * theta;

	speed_mps = distance / timeDelta
	speed_mps[np.isnan(speed_mps)] = 0
	speed_mps[speed_mps == np.inf] = 0
	return speed_mps
