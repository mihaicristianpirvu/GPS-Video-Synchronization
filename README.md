
Manual syncing
- main_export_altitude
 => Given a format of types ("matrice_100_logs", "standard_gpgga", ...(TODO)), exports an alitutde.npy file
- main_export_gps
 => Given a format of types ("standard_gnrmc", "standard_gpgga", "matrice_100_logs_nots", "matrice_100_logs", \
 	"standard_gngga"), exports a gps.npy file
- main_export_optical_flow
 => Given an movie (.mp4), exports the optical flow using PwC-Net
- main_match_signals
 => Given the three .npy files (altitude, gps, flow), sync them using the three predicted curves. Flow frequency is
  required (TODO, perhaps take it from movie somehow)

Automatic syncing of two separate GPS sources
 - main_sync_multiple_gps
 => Given two gps sources ("standard_gnrmc", "standard_gpgga", "matrice_100_logs_nots", "matrice_100_logs", \
 	"standard_gngga"), export a csv and a npy file using the two combined sets.
 => An output frequency is expected (usually the frequency of the target video).

 