import matplotlib.pyplot as plt
import numpy as np

def plot_signal(signal, label=""):
	plt.plot(np.arange(len(signal)), signal, label=label)
