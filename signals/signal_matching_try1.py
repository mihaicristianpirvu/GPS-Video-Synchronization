import numpy as np
from .utils import plot_signal
import matplotlib.pyplot as plt
from scipy.signal import correlate

def simplifySignal(signal):
	# Find peaks/outliers
	gradSignal = np.abs(np.gradient(signal))
	outliers = np.float32(gradSignal > 4 * np.mean(gradSignal))

	# Find continuous zones and make a median filter in them, so we make a "step" signal out of our noisy one
	where = np.where(np.gradient(outliers) != 0)[0]
	new_signal = np.copy(signal)
	for i in range(len(where) - 1):
		l, r = where[i], where[i + 1]
		if r - l == 1:
			continue
		new_signal[l : r] = np.median(signal[l : r])
	return new_signal

def find_peaks(signal):
	left = np.append(signal[1 : ], 0)
	right = np.append(0, signal[: -1])
	grad = (np.abs(signal - left) + np.abs(right - signal)) / 2

	value = 1e-7
	while True:
		outliers = np.float32(grad > value)
		where = np.where(outliers != 0)[0]
		if len(where) < len(signal) // 100:
			value /= 10
			break
		value *= 10
	outliers = np.float32(grad > value)
	where = np.where(outliers != 0)[0]
	return where

def find_peak_pairs(signal, peaks):
	gradSignal = np.gradient(signal)
	pairs = []
	for i in range(len(peaks) - 1):
		l, r = peaks[i], peaks[i + 1]
		if r - l < len(signal) // 100:
			continue
		# print(l, r, np.sum(np.abs(gradSignal[l : r])))
		if np.sum(np.abs(gradSignal[l : r])) < 0.1:
			pairs.append((l, r))

	# for pair in pairs:
	# 	plt.plot(pair[0], signal[pair[0]], "o")
	# 	plt.plot(pair[1], signal[pair[1]], "x")
	# plt.show()
	return pairs

def matchSignals(signal1, signal2):
	prime_signal1 = simplifySignal(signal1)
	grad_prime_signal1 = np.gradient(prime_signal1)
	# K = 5109
	K = 0
	plot_signal(prime_signal1[K :])
	# plot_signal(grad_prime_signal1)

	prime_signal2 = simplifySignal(signal2)
	grad_prime_signal2 = np.gradient(prime_signal2)
	peaks = find_peaks(prime_signal2)
	peak_pairs = find_peak_pairs(prime_signal2, peaks)
	plot_signal(prime_signal2)
	# plot_signal(grad_prime_signal2)

	# for l, r in peak_pairs:
	# 	plt.plot(l, prime_signal2[l], "o")
	# 	plt.plot(r, prime_signal2[r], "x")
	# 	plt.plot(l, prime_signal1[K : K + len(prime_signal2)][l], "o")
	# 	plt.plot(r, prime_signal1[K : K + len(prime_signal2)][r], "x")
	# plt.show()

	N = len(prime_signal1) - len(prime_signal2)
	res = np.zeros((N, ))
	for i in range(N):
		X = grad_prime_signal1[i : i + len(prime_signal2)]
		Sum = 0
		for pair in peak_pairs:
			l, r = pair
			value = np.abs(np.sum(correlate(X[l : r], grad_prime_signal2[l : r])))
			# value = np.sum(np.abs(X[l : r] - grad_prime_signal2[l : r]) < 1e-3)
			# value2 = np.sum( (np.abs(X[l : r]) > 1e-3) * (np.abs(grad_prime_signal2[l : r]) > 1e-3) )
			Sum += value
		res[i] = Sum
		print("%d/%d Corr: %2.5f" % (i, N, Sum))
	plot_signal((res - np.min(res)) / (np.max(res) - np.min(res)))
	plt.show()
	return np.argmax(res)
