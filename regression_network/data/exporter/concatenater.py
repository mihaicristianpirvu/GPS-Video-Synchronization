import h5py
import numpy as np

files = ["dataset_5_1.h5", "dataset_5_2.h5", "dataset_5_3.h5", "dataset_5_4.h5", "dataset_6_1.h5", "dataset_6_2.h5", \
	"dataset_6_3.h5", "dataset_6_4.h5"]

datas = [h5py.File(x) for x in files]
out = h5py.File("dataset.h5", "w")
out.create_group("train")
out.create_group("validation")

for type in ["train", "validation"]:
	print(type)
	trainDatas = [data[type] for data in datas]
	trainRGB = [x["rgb"] for x in trainDatas]
	trainGPS = [x["gps"] for x in trainDatas]

	numData = sum(len(x) for x in trainRGB)
	perm = np.random.permutation(numData)
	allData = np.concatenate(trainRGB[:], axis=0)
	allGPS = np.concatenate(trainGPS[:], axis=0)

	print("After concat:", allData.shape, allGPS.shape)

	allData = allData[perm]
	allGPS = allGPS[perm]
	print("Perm done")

	out[type]["rgb"] = allData
	out[type]["gps"] = allGPS
