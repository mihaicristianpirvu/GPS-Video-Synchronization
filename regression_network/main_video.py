import moviepy.editor as mpy
import pims
from functools import partial
from model_regression import ModelRegression
from loss import regression_loss, meter_metric_lat, meter_metric_lon
from argparse import ArgumentParser
from neural_wrappers.pytorch import maybeCuda
import numpy as np
from reader import computeDotMap
import matplotlib.pyplot as plt
from PIL import Image

def make_frame(t, model, video, gps, backgroundImage):
	t = min(int(t * video.frame_rate), len(video) - 1)
	currentFrame = np.expand_dims(np.array(video[t]), axis=0)
	currentFrame = np.float32(currentFrame) / 255
	thisLabels = np.expand_dims(gps[t], axis=0)

	thisResults = model.npForward(currentFrame)

	resDot = computeDotMap(thisResults)[0]
	labelDot = computeDotMap(thisLabels)[0]

	finalImage = np.copy(backgroundImage)

	whereRes = np.where(resDot == 1)
	whereLabel = np.where(labelDot == 1)
	whereBoth = np.where((resDot == 1) & (labelDot == 1))
	# Fix coordinates :)
	whereRes = (189 - whereRes[0], whereRes[1])
	whereLabel = (189 - whereLabel[0], whereLabel[1])
	whereBoth = (189 - whereBoth[0], whereBoth[1])

	finalImage[whereRes] = (255, 0, 0)
	finalImage[whereLabel] = (0, 0, 255)
	finalImage[whereBoth] = (0, 255, 0)

	# print("Lat error (m): ", meter_metric_lat(thisResults, thisLabels))
	# print("Lon error (m): ", meter_metric_lon(thisResults, thisLabels))
	# plt.imshow(currentFrame[0])
	# plt.figure()
	# plt.imshow(finalImage)
	# plt.show(True)

	return finalImage

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("in_video")
	parser.add_argument("in_gps")
	parser.add_argument("out_video")

	parser.add_argument("weights_file")
	parser.add_argument("--background_image")

	args = parser.parse_args()
	return args

def main():
	args = getArgs()

	gps = np.load(args.in_gps)[..., 0 : 2]
	mins = np.array([44.43448468573079, 26.045496832850702])
	maxes = np.array([44.436176574351016, 26.048940957214707])
	gps -= mins
	gps /= (maxes - mins)

	video = pims.Video(args.in_video)
	duration = len(video) // video.frame_rate
	model = maybeCuda(ModelRegression())
	criterion = regression_loss
	model.loadWeights(args.weights_file)

	if not args.background_image:
		backgroundImage = np.zeros((190, 270, 3), dtype=np.float32)
	else:
		backgroundImage = np.uint8(np.array(Image.open(args.background_image)))[..., 0 : 3]

	frameCallback = partial(make_frame, model=model, video=video, gps=gps, backgroundImage=backgroundImage)
	clip = mpy.VideoClip(frameCallback, duration=duration)
	ffmpeg_params = ["-crf", "15", "-preset", "veryslow", "-tune", "film"]
	clip.write_videofile(args.out_video, fps=video.frame_rate, verbose=False, \
		progress_bar=True, ffmpeg_params=ffmpeg_params)

if __name__ == "__main__":
	main()