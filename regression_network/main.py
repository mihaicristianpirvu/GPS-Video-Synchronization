import sys, os
from argparse import ArgumentParser
import matplotlib.pyplot as plt
from PIL import Image

import torch.optim as optim
import torch as tr
import numpy as np
from neural_wrappers.models import ModelUNetDilatedConv
from neural_wrappers.pytorch import maybeCuda
from neural_wrappers.callbacks import SaveModels, SaveHistory, PlotMetricsCallback, Callback

from model_regression import ModelRegression
from model_dot import ModelDot
from loss import regression_loss, bce_dice_loss, dice_coeff, np_dice_coeff, meter_metric_lat, meter_metric_lon
from reader import PoliFlightReader, PoliFlightDotReader, computeDotMap

def changeDirectory(Dir, expectExist):
	assert os.path.exists(Dir) == expectExist
	print("Changing to working directory:", Dir)
	if expectExist == False:
		os.makedirs(Dir)
	os.chdir(Dir)

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("type")
	parser.add_argument("dataset_path")
	parser.add_argument("model")

	parser.add_argument("--dir")
	parser.add_argument("--weights_file")
	parser.add_argument("--batch_size", type=int, default=100)
	parser.add_argument("--num_epochs", type=int, default=50)

	parser.add_argument("--test_background_image")
	parser.add_argument("--test_plot_results", type=int, default=0)

	args = parser.parse_args()
	assert args.type in ("train", "test", "retrain")
	assert args.model in ("model_regression", "model_dot")
	if args.type in ("test", "retrain"):
		assert not args.weights_file is None
		args.weights_file = os.path.abspath(args.weights_file)
	if not args.test_background_image is None:
		args.test_background_image = os.path.abspath(args.test_background_image)
	args.test_plot_results = bool(args.test_plot_results)

	return args

class DotCallack(Callback):
	def __init__(self, filePath):
		if not filePath is None:
			self.backgroundImage = np.uint8(np.array(Image.open(filePath)))[..., 0 : 3]
		else:
			self.backgroundImage = np.zeros((190, 270, 3), dtype=np.uint8)

	def onIterationEnd(self, **kwargs):
		inputs = kwargs["data"]
		results = kwargs["results"]
		labels = kwargs["labels"]
		metrics = kwargs["metrics"]

		MB = labels.shape[0]
		for i in range(MB):
			thisResults = results[i : i + 1]
			thisLabels = labels[i : i + 1]

			# First case is regression network, second is classification (dot) network.
			if results.shape[-1] == 2:
				resDot = computeDotMap(thisResults)[0]
				labelDot = computeDotMap(thisLabels)[0]
			else:
				resDot = np.uint8(thisResults[0] > 0.5)
				labelDot = thisLabels[0]

			finalImage = np.copy(self.backgroundImage)

			whereRes = np.where(resDot == 1)
			whereLabel = np.where(labelDot == 1)
			whereBoth = np.where((resDot == 1) & (labelDot == 1))
			# Fix coordinates :)
			whereRes = (189 - whereRes[0], whereRes[1])
			whereLabel = (189 - whereLabel[0], whereLabel[1])
			whereBoth = (189 - whereBoth[0], whereBoth[1])

			finalImage[whereRes] = (255, 0, 0)
			finalImage[whereLabel] = (0, 0, 255)
			finalImage[whereBoth] = (0, 255, 0)

			print("Lat error (m): ", meter_metric_lat(thisResults, thisLabels))
			print("Lon error (m): ", meter_metric_lon(thisResults, thisLabels))
			plt.imshow(inputs[i])
			plt.figure()
			plt.imshow(finalImage)
			plt.show(True)

def main():
	args = getArgs()

	MB = args.batch_size

	if args.model == "model_regression":
		reader = PoliFlightReader(args.dataset_path)
		model = ModelRegression()
		criterion = regression_loss
		metrics = {"Lat error (m)" : meter_metric_lat, "Lon error (m)" : meter_metric_lon}
	else:
		reader = PoliFlightDotReader(args.dataset_path)
		model = ModelDot(numFilters=32)
		criterion = bce_dice_loss
		metrics = {"Dice coeff" : np_dice_coeff}

	valGenerator = reader.iterate("validation", miniBatchSize=MB, maxPrefetch=0)
	valSteps = reader.getNumIterations("validation", miniBatchSize=MB)

	model = maybeCuda(model)
	model.setOptimizer(optim.Adam, lr=0.00001)
	model.setCriterion(criterion)
	model.setMetrics(metrics)
	print(model.summary())

	if args.type == "train":
		changeDirectory(args.dir, expectExist=False)
		model.train()
		generator = reader.iterate("train", miniBatchSize=MB, maxPrefetch=1)
		steps = reader.getNumIterations("train", miniBatchSize=MB)
		callbacks = [SaveModels(type="all"), SaveHistory("history.txt", mode="write"), \
			PlotMetricsCallback(["Loss"], ["min"])]

		model.train_generator(generator, steps, numEpochs=args.num_epochs, callbacks=callbacks, \
			validationGenerator=valGenerator, validationSteps=valSteps)

	elif args.type == "retrain":
		changeDirectory(args.dir, expectExist=True)
		model.train()
		generator = reader.iterate("train", miniBatchSize=MB, maxPrefetch=1)
		steps = reader.getNumIterations("train", miniBatchSize=MB)

		model.loadModel(args.weights_file)
		model.train_generator(generator, steps, numEpochs=args.num_epochs, callbacks=None, \
			validationGenerator=valGenerator, validationSteps=valSteps)

	elif args.type == "test":
		changeDirectory(args.dir, expectExist=True)
		model.eval()
		model.loadModel(args.weights_file)

		callbacks = []
		if args.test_plot_results:
			callbacks = [DotCallack(args.test_background_image)]

		metrics = model.test_generator(valGenerator, valSteps, callbacks=callbacks)
		print(metrics)

if __name__ == "__main__":
	main()
