from argparse import ArgumentParser
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from io import StringIO

# from main_export_gps import getGpsData

from main_export_gps import getGpsData
from main_match_signals import syncFrequencies

def getGpsData_V2(inFile, gpsFormat):
	f = open(inFile, "r", encoding = "ISO-8859-1")
	lines = f.readlines()
	for i in range(len(lines)):
		if lines[i].startswith("timestamp,latitude"):
			break
	lines = lines[i :]
	if gpsFormat == "matrice_100_logs":
		gpsData = parseGPSMatrice100_v2(lines)
	else:
		assert False
	return np.array(gpsData)

def parseGPSMatrice100_v2(rawGPSData):
	rawGPSData = StringIO("".join(rawGPSData))
	a = pd.read_csv(rawGPSData)
	a["timestamp"] = a["timestamp"].apply(lambda x : round(x / 10**9, 2))
	a["latitude"] = a["latitude"].apply(lambda x : x / np.pi * 180)
	a["longitude"] = a["longitude"].apply(lambda x : x / np.pi * 180)
	return a

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("in_file_1")
	parser.add_argument("in_file_2")
	parser.add_argument("export_frequency", type=float)
	parser.add_argument("--export_gps")
	parser.add_argument("--gps_format_1", default="matrice_100_logs")
	parser.add_argument("--gps_format_2", default="standard_gngga")

	parser.add_argument("--plot_gps", type=int, default=0)
	parser.add_argument("--plot_altitude", type=int, default=0)

	args = parser.parse_args()
	args.plot_gps = bool(args.plot_gps)
	args.plot_altitude = bool(args.plot_altitude)
	return args

def main():
	args = getArgs()

	gpsData1 = getGpsData(args.in_file_1, args.gps_format_1)
	gpsData2 = getGpsData(args.in_file_2, args.gps_format_2)
	print(gpsData1.shape, gpsData2.shape)

	x = np.array(gpsData1["timestamp"])
	frequency1 = np.median((1 / (x[1 :] - x[0 : -1])))
	frequency2 = np.median(1 / (gpsData2[1 : , 0] - gpsData2[0 : -1, 0]))
	print("freq1", frequency1, "freq2", frequency2)

	keys = list(gpsData1.keys())[1:]
	gpsData1 = pd.DataFrame({key : syncFrequencies(gpsData1[key], currentFreq=frequency1, \
		desiredFreq=args.export_frequency) for key in keys})
	items = []
	for i in range(gpsData2.shape[1]):
		items.append(syncFrequencies(gpsData2[..., i], currentFreq=frequency2, desiredFreq=args.export_frequency))
	gpsData2 = np.array(items).swapaxes(0, 1)

	print(gpsData1.shape, gpsData2.shape)

	numItems = min(len(gpsData1), len(gpsData2))
	gpsData1 = gpsData1[0 : numItems]
	gpsData2 = gpsData2[0 : numItems]

	finalDf = gpsData1.copy()
	finalDf["lat_external"] = gpsData2[..., 1]
	finalDf["lon_external"] = gpsData2[..., 2]
	finalDf["alt_external"] = gpsData2[..., 3]
	print(finalDf.keys())
	print(finalDf.shape)

	if not args.export_gps is None:
		finalDf.to_csv(args.export_gps)

	if args.plot_altitude:
		finalDf["altitude"].plot()
		finalDf["alt_external"].plot()

	if args.plot_gps:
		plt.figure()
		plt.plot(finalDf["longitude"], finalDf["latitude"])
		plt.plot(finalDf["lon_external"], finalDf["lat_external"])

	if args.plot_altitude or args.plot_gps:
		plt.show()

if __name__ == "__main__":
	main()